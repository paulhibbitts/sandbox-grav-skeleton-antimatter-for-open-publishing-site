---
title: Modular Page
onpage_menu: false
body_classes: "modular"

content:
    items: @self.modular
    order:
        by: default
        dir: asc
---

This is an example **modular** page, where content from separate **child** pages is displayed. Each of the below definitions are contained in a separate page, and these pages can be included in multiple areas of the site. [Text](http://wikieducator.org/OER_Handbook/educator_version_one/Conclusion/Glossary) by [http://wikieducator.org/](http://wikieducator.org/), available under a Creative Commons Attribution 4.0 license.
