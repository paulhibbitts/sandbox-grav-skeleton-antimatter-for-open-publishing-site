---
title: Search
published: true
hide_git_sync_repo_link: false
process:
    markdown: true
    twig: true
content:
    items: '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
icon: search
---

