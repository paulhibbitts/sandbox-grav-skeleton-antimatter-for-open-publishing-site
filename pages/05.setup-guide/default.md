---
title: 'Setup Guide'
published: false
hide_git_sync_repo_link: false
hide_git_repo_link: true
---

## It's as easy as 1-2-3 to publish, share and collaboratively edit your Grav site content!

1. Run the setup wizard for the [GitSync Plugin](../../admin/plugins/git-sync) and follow the provided instructions.
2. Configure the [Git Sync Link](../../admin/themes/mytheme) settings in the Theme options to change the display and functionality of the automatically displayed Git Sync Link.
3. If you would like, you can display a [Creative Commons(CC) Attribution License](../../admin/themes/mytheme) using the Theme options as well.
